// properties: orientation, position
// give we have Rover in position (x=3, y=2) facing N in a planet(context);
// when recieve forward (f) command;
// then rover new position is (x=4, y=2) facing N;

const NORTH = 'N';

class Rover {
  constructor() {
    this.position = { x: 1, y: 1 };
    this.orientation = NORTH;
  }

  forward() {}
}

describe('Rover', () => {
  test('should init', () => {
    const rover = new Rover();
    expect(rover.position).toEqual({ x: 1, y: 1 });
    expect(rover.orientation).toEqual(NORTH);
  });
  test('should move 1Y if orientation is NORTH', () => {
    const rover = new Rover();
    rover.forward();
    expect(rover.position).toEqual({ x: 1, y: 2 });
  });
});
