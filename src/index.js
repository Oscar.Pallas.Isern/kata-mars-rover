require('dotenv/config');

const gracefulShutdown = require('http-graceful-shutdown');

const express = require('express');

const routes = require('./appGateway/routes');

const app = express();

const router = express.Router();
app.use(routes(router));

app.listen(8000, () => {
  console.log(`App listening on port 8000!`);
});

gracefulShutdown(app, {
  finally: () => {
    console.log('Server gracefully shut down...');
  },
});
