module.exports = {
  extends: [
    'airbnb-base',
    'plugin:import/errors',
    'plugin:import/warnings',
    'prettier',
    'plugin:node/recommended',
  ],
  parserOptions: {
    ecmaVersion: 2020,
  },
  plugins: ['prettier', 'jest'],
  env: {
    es6: true,
    node: true,
    jest: true,
  },
  settings: {},
  globals: {
    rabbit: true,
    redisClients: true,
    logger: false,
  },
  rules: {
    'prettier/prettier': [
      'error',
      {
        singleQuote: true,
        printWidth: 100,
        trailingComma: 'all',
        arrowParens: 'avoid',
      },
    ],
    'no-console': 0,
    'no-new': 0,
    'no-await-in-loop': 0,
    'no-restricted-syntax': 0,
    'no-nested-ternary': 0,
    'no-underscore-dangle': 0,
    'import/prefer-default-export': 0,
    'import/no-dynamic-require': 0,
    'array-callback-return': 1,
    'no-return-assign': 0,
    'no-param-reassign': 0,
    'global-require': 0,
    'import/no-extraneous-dependencies': [
      'error',
      {
        devDependencies: true,
        optionalDependencies: false,
        peerDependencies: false,
      },
    ],
    'node/no-unpublished-require': [
      'error',
      {
        allowModules: ['node-mocks-http', 'mockdate'],
      },
    ],
    'arrow-parens': ['error', 'as-needed'],
    'default-param-last': ['warn'],
  },
};
